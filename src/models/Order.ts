export interface Order {
  order_id: number,
  order_date: string,
  customer: Customer,
  items: Array<Item>,
  shipping_price: number,
  discounts: Array<Discount>
}

export interface Item {
  quantity: number,
  unit_price: number,
  product: Product
}

export interface Product {
  product_id: string
}

export interface Customer {
  customer_id: number,
  shipping_address: {
    state: string
  }
}

export interface Discount {
  type: "DOLLAR" | "PERCENTAGE",
  value: number,
  priority: number
}

export interface OrderResponse {
  order_id: number
  order_datetime: string
  total_order_value: number
  average_unit_price: number
  distinct_unit_count: number
  total_units_count: number
  customer_state: string
}