import "reflect-metadata";
import { container } from "tsyringe";
import yargs from 'yargs/yargs';
import { hideBin } from 'yargs/helpers';
import { OrderService } from "./services/OrderService";
import { Config } from "./config/Config";

yargs(hideBin(process.argv))
  .command('run', 'summarise the orders', (yargs) => {
    return yargs
  }, (argv) => {
    const config = container.resolve(Config);
    
    // set inputs    
    if (argv.outdir) config.outputFolder = argv.outdir as string;
    if (argv.format) config.outputFormat = argv.format as string;

    const orderService = container.resolve(OrderService);
    orderService.transformOrder(argv.outfile as string | undefined);    
  })
  .option('outdir', {
    alias: 'd',
    type: 'string',
    description: 'The output directory (e.g. ./out)'
  })
  .option('outfile', {
    alias: 'o',
    type: 'string',
    description: 'The name of the output file'
  })
  .option('format', {
    alias: 'f',
    type: 'string',
    description: 'The format of the output (csv/xml/json)'
  })
  .parse()

