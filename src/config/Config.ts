import { singleton } from "tsyringe";

@singleton()
export class Config {
  public outputFolder = process.env.OUTPUT_FOLDER ?? "./out/";
  public outputFormat = process.env.OUTPUT_FORMAT ?? "csv";
  public bucketUrl = process.env.BUCKET_URL ?? "https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1/orders.jsonl";
  public csvColumns = ["order_id", "order_datetime", "total_order_value", "average_unit_price", "distinct_unit_count", "total_units_count", "customer_state"];
}