import { pipeline } from "stream/promises";
import { inject, injectable } from "tsyringe";
import jsonlines from 'jsonlines';
import * as csv from 'csv';
import { FileRepository } from "../repository/FileRepository";
import { S3Repository } from "../repository/S3Repository";
import { SummariseOrder } from "../transforms/SummariseOrder";
import { Config } from "../config/Config";
import { XmlTransform } from "../transforms/XmlTransform";

@injectable()
export class OrderService {
  constructor(
    @inject(S3Repository) private s3Repository: S3Repository,
    @inject(FileRepository) private fileRepository: FileRepository,
    @inject(Config) private config: Config
  ) {
    //
  }

  public transformOrder = async (outputfile: string | undefined) => {
    const readStream = await this.s3Repository.getReadStream();
    const writeStream = this.fileRepository.getWriteStream(outputfile);
    const parser = jsonlines.parse();
    // select and output transformer
    const outputTransformer = this.getOutputTransformer()

    await pipeline(
      readStream, // read from S3
      parser, // parse jsonline to JSON
      new SummariseOrder({}, this.config.outputFormat), // perform calculations on order
      outputTransformer, // prepare output for writing to file
      writeStream // write to file
    );
    console.log('Pipeline succeeded.');
  }

  private getOutputTransformer = () => {
    const columns = this.config.csvColumns;

    switch (this.config.outputFormat) {
      case "csv": return csv.stringify({ header: true, columns });
      // case "json": return undefined; // TODO
      case "xml": return new XmlTransform();
      default: return csv.stringify({ header: true, columns });
    }      
  }
}