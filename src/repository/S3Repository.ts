import axios from "axios";
import { injectable, inject } from "tsyringe";
import { Config } from "../config/Config";

@injectable()
export class S3Repository {
  constructor(
    @inject(Config) private config: Config
  ) {
    //
  }

  public getReadStream = async () => {
    try {
      const response = await axios.get(this.config.bucketUrl, {
        responseType: 'stream'
      });
      return response.data;
    } catch (e) {
      console.log("Error retrieving file", { error: e, bucketUrl: this.config.bucketUrl })
      throw e;
    }
  }

}