import fs from "fs";
import { randomUUID } from "crypto";
import { injectable, inject } from "tsyringe";
import { Config } from "../config/Config";

@injectable()
export class FileRepository {
  constructor(
    @inject(Config) private config: Config
  ) {
    //
  }

  public getWriteStream = (fileName?: string) => {
    const dir = this.config.outputFolder;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    switch (this.config.outputFormat) {
      case "csv": return fs.createWriteStream(this.config.outputFolder + (fileName ?? `${randomUUID()}.csv`));
      case "json": return fs.createWriteStream(this.config.outputFolder + (fileName ?? `${randomUUID()}.json`));
      case "xml": return fs.createWriteStream(this.config.outputFolder + (fileName ?? `${randomUUID()}.xml`));
      default: return fs.createWriteStream(this.config.outputFolder + (fileName ?? `${randomUUID()}.txt`));
    }    
  }
}