import { Transform, TransformCallback, TransformOptions } from "stream";
import { Discount, Item, Order, OrderResponse } from "../models/Order";

export class SummariseOrder extends Transform {
  private outputFormat = "csv";

  constructor(opts?: TransformOptions, outputFormat = "csv") {
    super({ ...{ readableObjectMode: true, writableObjectMode: true }, ...opts });
    this.outputFormat = outputFormat;
  }

  _transform(chunk: Order, encoding: BufferEncoding, callback: TransformCallback) {
    const order: Order = chunk; //JSON.parse(chunk.toString());

    // The total number of units in the order
    const totalUnitsCount = this.countTotalUnits(order.items);

    // The average price of each unit in the order, in dollars
    const totalUnitsValue = this.calculateTotalUnitsValue(order.items);
    const averageUnitPrice = totalUnitsCount ? totalUnitsValue / totalUnitsCount : 0; // check for divide by zero

    // The dollar sum of all line items in the order, excluding shipping, with all discounts applied. Note, discounts do not apply to shipping    
    const totalDiscount = this.calculateTotalDiscount(totalUnitsValue, order.discounts);
    const totalOrderValue = totalUnitsValue - totalDiscount;

    const values: OrderResponse =
    {
      "order_id": order.order_id,
      "order_datetime": order.order_date,
      "total_order_value": totalOrderValue,
      "average_unit_price": averageUnitPrice,
      "distinct_unit_count": this.countUniqueUnits(order.items),
      "total_units_count": totalUnitsCount,
      "customer_state": order.customer.shipping_address.state
    };

    // exclude order records with zero total order value
    if (totalOrderValue > 0) {
      this.push(this.formatResponse(values));
    }

    callback();
  }

  private formatResponse = (values: OrderResponse) => {
    const arrResponse = [
      values.order_id,
      values.order_datetime,
      values.total_order_value,
      values.average_unit_price,
      values.distinct_unit_count,
      values.total_units_count,
      values.customer_state
    ];

    switch (this.outputFormat) {
      case "csv": return arrResponse;
      // case "json": return undefined; // TODO
      case "xml": return [values];
      default: return arrResponse;
    }
  }

  // calculate any discount for order
  private calculateTotalDiscount = (totalUnitsValue: number, discounts: Array<Discount>) => {
    // ensure priority
    const sortedDiscounts = discounts.sort((a, b) => a.priority - b.priority);
    return sortedDiscounts.reduce(
      (acc, curr) => {
        const discountedTotal = totalUnitsValue - acc;
        let discount = acc;
        if (curr.type === "DOLLAR") {
          // could possible take discounted total to a negative this is checked on the return
          discount = discount + curr.value;
        }
        if (curr.type === "PERCENTAGE") {
          // only discount as a percentage any "undiscounted" total        
          // if total minus discount is zero then this amount will be zero
          discount = discount + (discountedTotal / 100 * curr.value);
        }
        // if total minus discount is zero then we cannot discount more than the total
        return discountedTotal - discount > 0 ? discount : discountedTotal
      },
      0
    )
  }

  // The count of unique units contained in the order
  private countUniqueUnits = (items: Array<Item>) => {
    const uniqueUnits = new Set<string>(); // product_id
    for (const item of items) {
      uniqueUnits.add(item.product.product_id);
    }
    return uniqueUnits.size;
  }

  // The total number of units in the order
  private countTotalUnits = (items: Array<Item>) => items.reduce(
    (acc: number, curr: Item) => acc + curr.quantity,
    0
  )

  // The value of units in the order
  private calculateTotalUnitsValue = (items: Array<Item>) => items.reduce(
    (acc: number, curr: Item) => acc + (curr.quantity * curr.unit_price),
    0
  )
}