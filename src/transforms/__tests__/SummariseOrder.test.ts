import { Readable } from "stream";
import { Order } from "../../models/Order";
import { SummariseOrder } from "../SummariseOrder";

describe('input json to formatted output stream', () => {

  const orders: Array<Record<string, unknown>> = [
    { "order_id": 1111, "order_date": "Sun, 10 Mar 2019 06:30:02 +0000", "customer": { "customer_id": 7652056, "first_name": "Electa", "last_name": "Okuneva", "email": "electa.okuneva@example.com", "phone": "4471.8531", "shipping_address": { "street": "39 WILLIAMSON AVENUE", "postcode": "3041", "suburb": "STRATHMORE", "state": "VICTORIA" } }, "items": [{ "quantity": 6, "unit_price": 59.95, "product": { "product_id": 3680410, "title": "JETS Women's E/F Cup Underwire One Piece - Teal", "subtitle": null, "image": "https://s.catch.com.au/images/product/0018/18211/5c820ebd01823684832373.jpg", "thumbnail": "https://s.catch.com.au/images/product/0018/18211/5c820ebd01823684832373_w200.jpg", "category": ["FASHION APPAREL", "APPAREL - WOMENS", "SWIMWEAR", "ONE PIECE"], "url": "https://www.catch.com.au/product/jets-womens-e-f-cup-underwire-one-piece-teal-3680410", "upc": "9326659633583", "gtin14": null, "created_at": "2019-02-07 16:54:39", "brand": { "id": 197636, "name": "Jets" } } }, { "quantity": 1, "unit_price": 14.99, "product": { "product_id": 3823397, "title": "HIIT: High Intensity Intercourse Training Book", "subtitle": null, "image": "https://s.catch.com.au/images/product/0019/19294/5cb50f537178e963271063.jpg", "thumbnail": "https://s.catch.com.au/images/product/0019/19294/5cb50f537178e963271063_w200.jpg", "category": ["ADULT", "ACCESSORIES", "BOOKS", "BOOKS ADULT"], "url": "https://www.catch.com.au/product/hiit-high-intensity-intercourse-training-book-3823397", "upc": "9781529102819", "gtin14": null, "created_at": "2019-03-20 12:59:14", "brand": { "id": 4085, "name": "Penguin" } } }], "discounts": [{ "type": "DOLLAR", "value": 6, "priority": 1 }, { "type": "PERCENTAGE", "value": 8, "priority": 2 }], "shipping_price": 10.99 },
    { "order_id": 2222, "order_date": "Sun, 10 Mar 2019 07:33:17 +0000", "customer": { "customer_id": 2665355, "first_name": "Melody", "last_name": "O'Kon", "email": "melody.okon@example.net", "phone": "(02)80612592", "shipping_address": { "street": "10 STELLA ST", "postcode": "3807", "suburb": "BEACONSFIELD", "state": "QUEENSLAND" } }, "items": [{ "quantity": 1, "unit_price": 69.99, "product": { "product_id": 3753308, "title": "Star Performers Series Super Star - Flesh", "subtitle": null, "image": "https://s.catch.com.au/images/product/0018/18485/5c9203aceea6e142991290.jpg", "thumbnail": "https://s.catch.com.au/images/product/0018/18485/5c9203aceea6e142991290_w200.jpg", "category": ["ADULT", "ADULT - WOMEN", "TOYS", "DILDOS & DONGS ADULT"], "url": "https://www.catch.com.au/product/star-performers-series-super-star-flesh-3753308", "upc": "788866010042", "gtin14": null, "created_at": "2019-03-01 12:25:16", "brand": { "id": 8568, "name": "Topco" } } }, { "quantity": 1, "unit_price": 12, "product": { "product_id": 3789758, "title": "2 x Capri Antibacterial All Purpose Wipes 20pk", "subtitle": null, "image": "https://s.catch.com.au/images/product/0018/18792/5ca1a75f0f55a633580709.jpg", "thumbnail": "https://s.catch.com.au/images/product/0018/18792/5ca1a75f0f55a633580709_w200.jpg", "category": ["HOUSEHOLD", "BATHROOM", "BATHROOM", "BATHROOM WIPES"], "url": "https://www.catch.com.au/product/2-x-capri-antibacterial-all-purpose-wipes-20pk-3789758", "upc": null, "gtin14": null, "created_at": "2019-03-12 10:57:01", "brand": { "id": 101109, "name": "Capri" } } }, { "quantity": 1, "unit_price": 24.99, "product": { "product_id": 3504503, "title": "Unit Salvage Flatpeak Snapback Cap - Black", "subtitle": null, "image": "https://s.catch.com.au/images/product/0017/17172/5c52d5541436b733526060.jpg", "thumbnail": "https://s.catch.com.au/images/product/0017/17172/5c52d5541436b733526060_w200.jpg", "category": ["FASHION ACCESSORIES", "HATS", "HATS - MENS", "SNAP BACK"], "url": "https://www.catch.com.au/product/unit-salvage-flatpeak-snapback-cap-black-3504503", "upc": "9341913758994", "gtin14": null, "created_at": "2019-01-14 11:54:15", "brand": { "id": 4236, "name": "Unit" } } }, { "quantity": 4, "unit_price": 149, "product": { "product_id": 3919762, "title": "Fuck Friends Swinger Series Rosita Suave Love Doll", "subtitle": null, "image": "https://s.catch.com.au/images/product/0019/19321/5cb6e0dc23809754658880.jpg", "thumbnail": "https://s.catch.com.au/images/product/0019/19321/5cb6e0dc23809754658880_w200.jpg", "category": ["ADULT", "ADULT - MEN", "TOYS", "DOLLS ADULT"], "url": "https://www.catch.com.au/product/fuck-friends-swinger-series-rosita-suave-love-doll-3919762", "upc": "818631032785", "gtin14": null, "created_at": "2019-04-05 13:41:35", "brand": { "id": 31864, "name": "Hott Products Unlimited" } } }, { "quantity": 1, "unit_price": 54.99, "product": { "product_id": 3689908, "title": "Ardor Lucille Textured King Bed Quilt Cover Set - Grey", "subtitle": null, "image": "https://s.catch.com.au/images/product/0018/18000/5c793afa3717b780576000.jpg", "thumbnail": "https://s.catch.com.au/images/product/0018/18000/5c793afa3717b780576000_w200.jpg", "category": ["HOME", "BED LINEN", "QUILT COVERS", "QUILT COVER SETS"], "url": "https://www.catch.com.au/product/ardor-lucille-textured-king-bed-quilt-cover-set-grey-3689908", "upc": "9319288517768", "gtin14": null, "created_at": "2019-02-11 11:24:49", "brand": { "id": 3229, "name": "Ardor" } } }, { "quantity": 5, "unit_price": 24.99, "product": { "product_id": 3746255, "title": "Liquid Sex Tightening Cream For Her 56g ", "subtitle": null, "image": "https://s.catch.com.au/images/product/0018/18485/5c9203ad0253f590629520.jpg", "thumbnail": "https://s.catch.com.au/images/product/0018/18485/5c9203ad0253f590629520_w200.jpg", "category": ["ADULT", "ACCESSORIES", "ACCESSORIES", "SENSUAL OILS AND CREAMS ADULT"], "url": "https://www.catch.com.au/product/liquid-sex-tightening-cream-for-her-56g-3746255", "upc": "51021390984", "gtin14": null, "created_at": "2019-02-27 15:39:03", "brand": { "id": 8568, "name": "Topco" } } }], "discounts": [], "shipping_price": 8.99 }
  ];

  it('should transform a list of orders for csv stringifying', (done) => {
    const expectedResponse =
      [[1111, "Sun, 10 Mar 2019 06:30:02 +0000", 339.19480000000004, 53.52714285714286, 2, 7, "VICTORIA"], [2222, "Sun, 10 Mar 2019 07:33:17 +0000", 882.9200000000001, 67.91692307692308, 6, 13, "QUEENSLAND"]];

    let response: Array<any> = [];

    Readable.from(orders)
      .pipe(new SummariseOrder())
      .on('data', (result) => {
        response.push(result);
      })
      .on('end', () => {
        try {
          expect(response).toMatchObject(expectedResponse);
          done();
        } catch (error) {
          done(error);
        }
      });
  });

  it('should transform a list of orders to xml transforming', (done) => {
    const expectedResponse = [
      [{
        "order_id": 1111,
        "order_datetime": "Sun, 10 Mar 2019 06:30:02 +0000",
        "total_order_value": 339.19480000000004,
        "average_unit_price": 53.52714285714286,
        "distinct_unit_count": 2,
        "total_units_count": 7,
        "customer_state": "VICTORIA"
      }],
      [{
        "order_id": 2222,
        "order_datetime": "Sun, 10 Mar 2019 07:33:17 +0000",
        "total_order_value": 882.9200000000001,
        "average_unit_price": 67.91692307692308,
        "distinct_unit_count": 6,
        "total_units_count": 13,
        "customer_state": "QUEENSLAND"
      }]
    ]
    let response: Array<any> = [];

    Readable.from(orders)
      .pipe(new SummariseOrder({}, 'xml'))
      .on('data', (result) => {
        response.push(result);
      })
      .on('end', () => {
        try {
          console.log('streaming ended'); // just for debugging purposes
          expect(response).toMatchObject(expectedResponse);
          done();
        } catch (error) {
          done(error);
        }
      });
  });
});