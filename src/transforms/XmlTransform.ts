import { TransformCallback, Transform } from "stream";
import { createCB } from 'xmlbuilder2';
import { XMLBuilderCB } from "xmlbuilder2/lib/interfaces";

export class XmlTransform extends Transform {
  private xml: XMLBuilderCB;
  private row: string;
  constructor({
    root = "root",
    row = "row",
    prettyPrint = true,
    declaration = true,
  } = {}) {
    super({ objectMode: true });
    this.xml = createCB({
      data: (text: string) => this.push(text),
      prettyPrint,
    })
    this.row = row
    if (declaration) { this.xml.dec({ "encoding": "UTF-8" }) }
    this.xml.ele(root)
  }

  _transform(chunk: string, encoding: BufferEncoding, callback: TransformCallback) {
    this.xml.ele(this.row).ele(chunk).up()
    callback()
  }

  _flush(callback: TransformCallback) {
    this.xml.up().end()
    callback()
  }
}