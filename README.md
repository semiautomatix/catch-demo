# catch-demo

## About the author
Tim Gaul<br/>
https://gaul.dev<br/>
tim@gaul.dev<br/>

## Requirements

Requires `Node.js 16+` as recommended by AWS and `yarn 1.19.1+`

"We recommend that you upgrade your existing Node.js 12 functions to Node.js 16 before November 14, 2022."

## Installation

```
yarn 
```

## Getting started

```
index.ts [command]

Commands:
  index.ts run  summarise the orders

Options:
      --help     Show help                                             [boolean]
      --version  Show version number                                   [boolean]
  -d, --outdir   The output directory (e.g. ./out)                      [string]
  -o, --outfile  The name of the output file                            [string]
  -f, --format   The format of the output (csv/xml/json)                [string]
```

## TypeScript

Project is written in TypeScript, to make things easier there are convenience scripts.

To start the project (see options above):

```
yarn start run
```

To test the project:

```
yarn test
```

## What is included?

- TypeScript
- Linting
- Dependency Injection for IoC
- Example Unit Tests encouraging Test Driven Design (TDD)
- Code separation of concerns encourgaing Domain Driven Design (DDD)
- Asynchronous methods, soon to be top-level!
- Third party libraries:
  - axios
  - csv
  - jsonlines
  - xmlbuilder2
  - yargs

## What is missing?
- More and more units tests, always ensure your happy paths (at the very least are covered)
- Additional error logging and handling (the S3 repository has a good example)
- Any form of integration tests
- A CI/CD pipeline
- `json` output
- `yaml` output